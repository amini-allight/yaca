# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./constants"
require "./tools"
require "./http_tools"
require "./webrtc_session"
require "./webrtc_messages"

class WebRTCRouter
    @ice_servers = [] of ICEServer
    @sessions = {} of UInt64 => WebRTCSession

    def initialize
        if File.exists? ICE_SERVER_CONFIG_PATH
            @ice_servers = Array(ICEServer).from_json File.read(ICE_SERVER_CONFIG_PATH)
        else
            @ice_servers = [ DEFAULT_ICE_SERVER ]
        end
    end

    def ice(context : HTTP::Server::Context) : Void
        begin
            ok_response context, "application/json", ICEResponse.new(@ice_servers).to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def get_offer(context : HTTP::Server::Context) : Void
        begin
            request = GetOfferRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            offer = session.get_offer

            ok_response context, "application/json", GetOfferResponse.new(offer).to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def get_answer(context : HTTP::Server::Context) : Void
        begin
            request = GetAnswerRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            answer = session.get_answer

            ok_response context, "application/json", GetAnswerResponse.new(answer).to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def offer(context : HTTP::Server::Context) : Void
        begin
            request = OfferRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            session.offer request.local_description

            ok_response context, "application/json", EmptyResponse.new.to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def answer(context : HTTP::Server::Context) : Void
        begin
            request = OfferRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            session.answer request.local_description

            ok_response context, "application/json", EmptyResponse.new.to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def push_server_ice_candidate(context : HTTP::Server::Context) : Void
        begin
            request = PushICECandidateRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            session.push_server_ice_candidate request.candidate

            ok_response context, "application/json", EmptyResponse.new.to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def pull_server_ice_candidate(context : HTTP::Server::Context) : Void
        begin
            request = PullICECandidateRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            candidate = session.pull_server_ice_candidate

            ok_response context, "application/json", PullICECandidateResponse.new(candidate).to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def push_client_ice_candidate(context : HTTP::Server::Context) : Void
        begin
            request = PushICECandidateRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            session.push_client_ice_candidate request.candidate

            ok_response context, "application/json", EmptyResponse.new.to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def pull_client_ice_candidate(context : HTTP::Server::Context) : Void
        begin
            request = PullICECandidateRequest.from_json context.request.body.as(IO)

            session = ensure_session request.id

            candidate = session.pull_client_ice_candidate

            ok_response context, "application/json", PullICECandidateResponse.new(candidate).to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    private def ensure_session(id : UInt64) : WebRTCSession
        session = @sessions[id]?

        return session if !session.nil?

        session = WebRTCSession.new id

        @sessions[id] = session

        session
    end
end
