# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "http/server"
require "./basic_messages"

def ok_response(context : HTTP::Server::Context, content_type : String, content : String) : Void
    context.response.status_code = 200
    context.response.status_message = "OK"
    context.response.content_type = content_type
    context.response.print content
end

def ok_response(context : HTTP::Server::Context) : Void
    context.response.status_code = 200
    context.response.status_message = "OK"
    context.response.print ""
end

def bad_request_response(context : HTTP::Server::Context) : Void
    context.response.status_code = 400
    context.response.status_message = "Bad Request"
    context.response.print ErrorResponse.new("Bad Request").to_json
end

def internal_server_error_response(context : HTTP::Server::Context) : Void
    context.response.status_code = 500
    context.response.status_message = "Internal Server Error"
    context.response.print ErrorResponse.new("Internal Server Error").to_json
end

def not_found_response(context : HTTP::Server::Context) : Void
    context.response.status_code = 404
    context.response.status_message = "Not Found"
    context.response.print ErrorResponse.new("Not Found").to_json
end

def service_unavailable_response(context : HTTP::Server::Context) : Void
    context.response.status_code = 503
    context.response.status_message = "Service Unavailable"
    context.response.print ErrorResponse.new("Service Unavailable").to_json
end

def error_response(context : HTTP::Server::Context, error : String) : Void
    context.response.status_code = 200
    context.response.status_message = "OK"
    context.response.print ErrorResponse.new(error).to_json
end
