# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "json"
require "./basic_messages"
require "./connection"

struct NewRequest
    property session : String

    include JSON::Serializable
end

struct JoinRequest
    property session : String
    property username : String

    include JSON::Serializable
end

struct LeaveRequest
    property session : String
    property username : String

    include JSON::Serializable
end

struct PingRequest
    property session : String
    property username : String

    include JSON::Serializable
end

struct PingResponse
    property usernames : Array(String)

    def initialize(@usernames : Array(String))

    end

    include JSON::Serializable
end

struct ConnectToRequest
    property session : String
    property src_username : String
    property dst_username : String

    include JSON::Serializable
end

struct ConnectToResponse
    property connection : Connection

    def initialize(@connection : Connection)

    end

    include JSON::Serializable
end
