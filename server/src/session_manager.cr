# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./tools"
require "./http_tools"
require "./session"
require "./session_messages"

class SessionManager
    @sessions = {} of String => Session

    def initialize

    end

    def new(context : HTTP::Server::Context) : Void
        begin
            request = NewRequest.from_json context.request.body.as(IO)

            if request.session.empty?
                error_response context, "Invalid Session Name"
                return
            end

            session = @sessions[request.session]?

            if !session.nil?
                ok_response context, "application/json", EmptyResponse.new.to_json
                return
            end

            session = Session.new request.session

            @sessions[request.session] = session

            ok_response context, "application/json", EmptyResponse.new.to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def join(context : HTTP::Server::Context) : Void
        begin
            request = JoinRequest.from_json context.request.body.as(IO)

            if request.session.empty?
                error_response context, "Invalid Session Name"
                return
            end

            if request.username.empty?
                error_response context, "Invalid Username"
                return
            end

            session = @sessions[request.session]?

            if session.nil?
                session = Session.new request.session
                @sessions[request.session] = session
            end

            session.join request.username

            ok_response context, "application/json", EmptyResponse.new.to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def leave(context : HTTP::Server::Context) : Void
        begin
            request = LeaveRequest.from_json context.request.body.as(IO)

            if request.session.empty?
                error_response context, "Invalid Session Name"
                return
            end

            if request.username.empty?
                error_response context, "Invalid Username"
                return
            end

            session = @sessions[request.session]?

            if session.nil?
                not_found_response context
                return
            end

            session.leave request.username

            ok_response context, "application/json", EmptyResponse.new.to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def ping(context : HTTP::Server::Context) : Void
        begin
            request = PingRequest.from_json context.request.body.as(IO)

            if request.session.empty?
                error_response context, "Invalid Session Name"
                return
            end

            if request.username.empty?
                error_response context, "Invalid Username"
                return
            end

            session = @sessions[request.session]?

            if session.nil?
                session = Session.new request.session
                @sessions[request.session] = session
            end

            if !session.users.includes? request.username
                session.join request.username
            end

            session.ping request.username

            ok_response context, "application/json", PingResponse.new(session.users).to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    def connect_to(context : HTTP::Server::Context) : Void
        begin
            request = ConnectToRequest.from_json context.request.body.as(IO)

            if request.session.empty?
                error_response context, "Invalid Session Name"
                return
            end

            session = @sessions[request.session]?

            if session.nil?
                not_found_response context
                return
            end

            if request.src_username.empty? || request.dst_username.empty?
                error_response context, "Invalid Username"
                return
            end

            connection = session.connect_to request.src_username, request.dst_username

            ok_response context, "application/json", ConnectToResponse.new(connection).to_json
        rescue e : Exception
            report_error e
            internal_server_error_response context
        end
    end

    private def clean : Void
        @sessions.reject! { |k, v| v.last.expired? }
    end
end
