#!/bin/crystal run
# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "http/server"
require "./constants"
require "./http_tools"
require "./session_manager"
require "./webrtc_router"

if ARGV.size != 2
    puts "Usage: ./src/main.cr -- 0.0.0.0 3000"
    exit 1
end

HOST = ARGV[0]
PORT = ARGV[1].to_i

SESSION_MANAGER = SessionManager.new
WEBRTC_ROUTER = WebRTCRouter.new

def handle_home(context : HTTP::Server::Context) : Void
    ok_response context, "text/html", File.read("#{RESOURCE_PATH}/index.html")
end

def handle_favicon(context : HTTP::Server::Context) : Void
    ok_response context, "image/x-icon", File.read("#{RESOURCE_PATH}/favicon.ico")
end

def handle_static(context : HTTP::Server::Context) : Void
    if context.request.path.includes? ".."
        not_found_response context
        return
    end

    file_path = context.request.path[STATIC_PREFIX.size, context.request.path.size - STATIC_PREFIX.size]

    full_path = "#{STATIC_PATH}/#{file_path}"

    if !File.exists? full_path
        not_found_response context
        return
    end

    content_type = ""

    case context.request.path
    when .ends_with? ".html"
        content_type = "text/html"
    when .ends_with? ".css"
        content_type = "text/css"
    when .ends_with? ".js"
        content_type = "application/ecmascript"
    when .ends_with? ".json"
        content_type = "application/json"
    when .ends_with? ".png"
        content_type = "image/png"
    when .ends_with? ".ttf"
        content_type = "font/ttf"
    when .ends_with? ".txt"
    else
        content_type = "text/plain"
    end

    file = File.read full_path

    ok_response context, content_type, file
end

def handle_arch(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/arch"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/arch.sh.ecr")
end

def handle_debian(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/debian"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/debian.sh.ecr")
end

def handle_rhel(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/rhel"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/rhel.sh.ecr")
end

def handle_windows(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/windows"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/windows.sh.ecr")
end

def handle_macos(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/macos"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/macos.sh.ecr")
end

def handle_freebsd(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/freebsd"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/freebsd.sh.ecr")
end

server = HTTP::Server.new do |context|
    case context.request.path
    when /^\/$/ then handle_home context
    when /^\/session\/new$/ then SESSION_MANAGER.new context
    when /^\/session\/join$/ then SESSION_MANAGER.join context
    when /^\/session\/leave$/ then SESSION_MANAGER.leave context
    when /^\/session\/ping$/ then SESSION_MANAGER.ping context
    when /^\/session\/connect-to$/ then SESSION_MANAGER.connect_to context
    when /^\/webrtc\/ice$/ then WEBRTC_ROUTER.ice context
    when /^\/webrtc\/get-offer$/ then WEBRTC_ROUTER.get_offer context
    when /^\/webrtc\/get-answer$/ then WEBRTC_ROUTER.get_answer context
    when /^\/webrtc\/offer$/ then WEBRTC_ROUTER.offer context
    when /^\/webrtc\/answer$/ then WEBRTC_ROUTER.answer context
    when /^\/webrtc\/push-server-ice-candidate$/ then WEBRTC_ROUTER.push_client_ice_candidate context
    when /^\/webrtc\/pull-server-ice-candidate$/ then WEBRTC_ROUTER.pull_client_ice_candidate context
    when /^\/webrtc\/push-client-ice-candidate$/ then WEBRTC_ROUTER.push_client_ice_candidate context
    when /^\/webrtc\/pull-client-ice-candidate$/ then WEBRTC_ROUTER.pull_client_ice_candidate context
    when /^\/favicon\.ico$/ then handle_favicon context
    when /^\/static\/.*$/ then handle_static context
    else not_found_response context
    end
end

puts "Server online at #{HOST}:#{PORT}"
server.listen HOST, PORT

