# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "json"
require "./basic_messages"
require "./ice_server"

struct ICEResponse
    property servers : Array(ICEServer)

    def initialize(@servers : Array(ICEServer))

    end

    include JSON::Serializable
end

struct GetOfferRequest
    property id : UInt64

    include JSON::Serializable
end

struct GetOfferResponse
    property remote_description : String?

    def initialize(@remote_description : String?)

    end

    include JSON::Serializable
end

struct GetAnswerRequest
    property id : UInt64

    include JSON::Serializable
end

struct GetAnswerResponse
    property remote_description : String?

    def initialize(@remote_description : String?)

    end

    include JSON::Serializable
end

struct OfferRequest
    property id : UInt64
    property local_description : String

    include JSON::Serializable
end

struct AnswerRequest
    property id : UInt64
    property local_description : String

    include JSON::Serializable
end

struct PushICECandidateRequest
    property id : UInt64
    property candidate : String

    include JSON::Serializable
end

struct PullICECandidateRequest
    property id : UInt64

    include JSON::Serializable
end

struct PullICECandidateResponse
    property candidate : String?

    def initialize(@candidate : String?)

    end

    include JSON::Serializable
end
