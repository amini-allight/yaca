# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./ice_server"

RESOURCE_PATH = "res"
STATIC_PATH = "#{RESOURCE_PATH}/static"
STATIC_PREFIX = "/static/"
USER_TIMEOUT = 10 * 1000
SESSION_TIMEOUT = 120 * 1000
WEBRTC_SESSION_TIMEOUT = 10 * 1000
{% if flag? :darwin %}
ICE_SERVER_CONFIG_PATH = "#{ENV["HOME"]}/Library/Application Support/Yaca/config.json"
{% elsif flag? :win32 %}
ICE_SERVER_CONFIG_PATH = "#{ENV["APPDATA"]}/Yaca/config.json"
{% else %}
ICE_SERVER_CONFIG_PATH = "#{ENV["HOME"]}/.config/yaca/config.json"
{% end %}
DEFAULT_ICE_SERVER = ICEServer.new [ "stun:stun.l.google.com:19302" ]
