# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./tools"
require "./constants"
require "./connection"

class User
    getter name
    getter connections = {} of String => Connection
    @last_seen : Int64

    def initialize(@name : String)
        @last_seen = current_time
    end

    def ping : Void
        @last_seen = current_time       
    end

    def expired? : Bool
        current_time - @last_seen > USER_TIMEOUT
    end

    def connect_to(other : User) : Connection
        if @connections[other.name]?.nil? || other.connections[@name]?.nil?
            id = Random.new.rand(UInt64)

            @connections[other.name] = Connection.new id, true
            other.connections[@name] = Connection.new id, false
        end

        @connections[other.name]
    end
end
