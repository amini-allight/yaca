# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./tools"
require "./constants"

class WebRTCSession
    getter id
    @server_ice_candidates = [] of String
    @client_ice_candidates = [] of String
    @offer : String? = nil
    @answer : String? = nil
    @last_seen : Int64

    def initialize(@id : UInt64)
        @last_seen = current_time
    end

    def expired? : Bool
        current_time - @last_seen > WEBRTC_SESSION_TIMEOUT
    end

    def get_offer : String?
        mark_seen

        offer = @offer

        @offer = nil

        offer
    end

    def get_answer : String?
        mark_seen

        answer = @answer

        @answer = nil

        answer
    end

    def offer(offer : String) : Void
        mark_seen

        @offer = offer
    end

    def answer(answer : String) : Void
        mark_seen

        @answer = answer
    end

    def push_server_ice_candidate(candidate : String) : Void
        mark_seen

        @server_ice_candidates << candidate
    end

    def pull_server_ice_candidate : String?
        mark_seen

        @server_ice_candidates.pop?
    end

    def push_client_ice_candidate(candidate : String) : Void
        mark_seen

        @client_ice_candidates << candidate
    end

    def pull_client_ice_candidate : String?
        mark_seen

        @client_ice_candidates.pop?
    end

    private def mark_seen : Void
        @last_seen = current_time
    end
end
