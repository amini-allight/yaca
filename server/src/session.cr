# Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
# Copyright (C) 2024 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./user"

class Session
    getter name
    @users = {} of String => User
    @last_seen : Int64

    def initialize(@name : String)
        @last_seen = current_time
    end

    def join(username : String) : Void
        @users[username] = User.new username
        clean
    end

    def leave(username : String) : Void
        @users.delete username
        clean
    end

    def ping(username : String) : Void
        @last_seen = current_time

        user = @users[username]

        raise Exception.new "invalid ping" if user.nil?

        user.ping

        clean
    end

    def expired? : Bool
        current_time - @last_seen > SESSION_TIMEOUT
    end

    def users : Array(String)
        @users.keys
    end

    def connect_to(src_username : String, dst_username : String) : Connection
        src = @users[src_username]?
        dst = @users[dst_username]?

        raise Exception.new "invalid connection request" if src.nil? || dst.nil?

        connection = src.connect_to dst

        clean

        connection
    end

    private def clean : Void
        @users.reject! { |k, v| v.expired? }
    end
end
