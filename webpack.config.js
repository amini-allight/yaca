const path = require("path");

module.exports = {
    mode: "development",
    entry: "./src/main.tsx",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: "/node_modules/"
            }
        ]
    },
    resolve: {
        extensions: [ ".tsx" ]
    },
    output: {
        filename: "main.js",
        path: path.resolve(__dirname, "server/res/static"),
    }
};

