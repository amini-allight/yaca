/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import * as React from "react";
import User from "./user";
import * as SessionMessages from "./session-messages";
import AudioInput from "./audio-input";
import AudioOutput from "./audio-output";
import WebRTCSocket from "./webrtc-socket";
import WebRTCServer from "./webrtc-server";
import WebRTCClient from "./webrtc-client";

const pingInterval: number = 500;

interface SessionProps
{
    session: string;
    username: string;
    onError: (error: string) => void;
}

interface SessionState
{
    users: Array<string>;
}

export default class Session extends React.Component<SessionProps, SessionState>
{
    sockets: { [key: string]: WebRTCSocket };
    audioInput: AudioInput;
    audioOutput: AudioOutput;

    constructor(props: SessionProps)
    {
        super(props);

        this.onToggleMute = this.onToggleMute.bind(this);
        this.onAudioInput = this.onAudioInput.bind(this);
        this.onAudioOutput = this.onAudioOutput.bind(this);
        this.onTick = this.onTick.bind(this);
        this.onPing = this.onPing.bind(this);
        this.onConnectionTo = this.onConnectionTo.bind(this);
        this.onSocketStatusChange = this.onSocketStatusChange.bind(this);

        this.state = {
            users: []
        };

        this.sockets = {};
        this.audioInput = new AudioInput(this.onAudioInput, this.props.onError);
        this.audioOutput = new AudioOutput();
    }

    render()
    {
        var users = [];

        for (var i = 0; i < this.state.users.length; i++)
        {
            var name = this.state.users[i];
            var socket = this.sockets[name];

            var connectionStatus = "pending";

            if (name == this.props.username)
            {
                connectionStatus = "myself";
            }

            if (socket)
            {
                connectionStatus = socket.connectionStatus();
            }

            users.push(<User key={name} name={name} connectionStatus={connectionStatus}/>);
        }

        var buttonClass = "";

        if (this.audioInput.muted())
        {
            buttonClass = "unmute";
        }
        else
        {
            buttonClass = "mute";
        }

        return (
            <div className="session">
                <button className="microphone-button" onClick={this.onToggleMute}>
                    <div className={buttonClass}></div>
                </button>
                <div className="users">
                    {users}
                </div>
            </div>
        );
    }

    private onToggleMute(): void
    {
        if (this.audioInput.muted())
        {
            this.audioInput.unmute();
        }
        else
        {
            this.audioInput.mute();
        }

        this.forceUpdate();
    }

    private onAudioInput(): void
    {
        setInterval(this.onTick, pingInterval);
    }

    private onAudioOutput(track: MediaStreamTrack): void
    {
        this.audioOutput.addTrack(track);
    }

    private onTick(): void
    {
        var request: SessionMessages.PingRequest = {
            session: this.props.session,
            username: this.props.username
        };

        fetch("/session/ping", {
            method: "POST",
            body: JSON.stringify(request)
        })
            .then(res => res.json())
            .then(this.onPing)
            .catch(e => this.props.onError(e.message));
    }

    private onPing(json: any): void
    {
        if (json.error)
        {
            this.props.onError(json.error);
            return;
        }

        var response: SessionMessages.PingResponse = json;

        this.setState({
            users: response.usernames
        });

        for (var i = 0; i < response.usernames.length; i++)
        {
            let username = response.usernames[i];

            if (username == this.props.username)
            {
                continue;
            }

            if (!this.sockets[username])
            {
                var request: SessionMessages.ConnectToRequest = {
                    session: this.props.session,
                    src_username: this.props.username,
                    dst_username: username
                };

                fetch("/session/connect-to", {
                    method: "POST",
                    body: JSON.stringify(request)
                })
                    .then(res => res.json())
                    .then((json => this.onConnectionTo(json, username)).bind(this))
                    .catch(e => this.props.onError(e.message));
            }
        }
    }

    private onConnectionTo(json: any, username: string): void
    {
        if (json.error)
        {
            this.props.onError(json.error);
            return;
        }

        var response: SessionMessages.ConnectToResponse = json;

        if (this.sockets[username])
        {
            return;
        }

        if (response.connection.server)
        {
            console.log("Creating a server to connect to '" + username + "'.");

            this.sockets[username] = new WebRTCServer(
                response.connection.id,
                this.audioInput.track,
                this.onAudioOutput,
                this.onSocketStatusChange,
                this.props.onError
            );
        }
        else
        {
            console.log("Creating a client to connect to '" + username + "'.");

            this.sockets[username] = new WebRTCClient(
                response.connection.id,
                this.audioInput.track,
                this.onAudioOutput,
                this.onSocketStatusChange,
                this.props.onError
            );
        }
    }

    private onSocketStatusChange(): void
    {
        this.forceUpdate();
    }
}
