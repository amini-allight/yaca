/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
export interface ICEServer
{
    urls: Array<string>;
    username: string;
    credential: string;
}

export interface ICEResponse
{
    servers: Array<ICEServer>;
}

export interface GetOfferRequest
{
    id: number;
}

export interface GetOfferResponse
{
    remote_description: string;
}

export interface GetAnswerRequest
{
    id: number;
}

export interface GetAnswerResponse
{
    remote_description: string;
}

export interface OfferRequest
{
    id: number;
    local_description: string;
}

export interface AnswerRequest
{
    id: number;
    local_description: string;
}

export interface PushICECandidateRequest
{
    id: number;
    candidate: string;
}

export interface PullICECandidateRequest
{
    id: number;
}

export interface PullICECandidateResponse
{
    candidate: string;
}
