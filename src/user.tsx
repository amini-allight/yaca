/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import * as React from "react";

interface UserProps
{
    name: string;
    connectionStatus: string;
}

interface UserState
{

}

export default class User extends React.Component<UserProps, UserState>
{
    constructor(props: UserProps)
    {
        super(props);
    }

    render()
    {
        var statusLightClasses = "user-status-light";

        switch (this.props.connectionStatus)
        {
        case "myself" :
        case "pending" :
            break;
        case "connected" :
            statusLightClasses += " user-status-light-ok";
            break;
        case "disconnected" :
            statusLightClasses += " user-status-light-error";
            break;
        }

        var statusLight = null;

        if (this.props.connectionStatus != "myself")
        {
            statusLight = (<div className={statusLightClasses}></div>);
        }

        return (
            <div className="user">
                <span className="user-name">{this.props.name}</span>
                {statusLight}
            </div>
        );
    }
}
