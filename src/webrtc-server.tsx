/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import WebRTCSocket from "./webrtc-socket";
import * as WebRTCMessages from "./webrtc-messages";

const answerPollInterval: number = 250;
const iceCandidatePollInterval: number = 250;

export default class WebRTCServer extends WebRTCSocket
{
    connection: RTCPeerConnection;
    inTrack: MediaStreamTrack;
    outTrack: MediaStreamTrack;
    onTrack: (track: MediaStreamTrack) => void;
    onStatusChange: () => void;
    onError: (error: string) => void;

    constructor(
        id: number,
        track: MediaStreamTrack,
        onTrack: (track: MediaStreamTrack) => void,
        onStatusChange: () => void,
        onError: (error: string) => void
    )
    {
        super(id);

        this.onICE = this.onICE.bind(this);
        this.onOffer = this.onOffer.bind(this);
        this.onLocalDescription = this.onLocalDescription.bind(this);
        this.getAnswer = this.getAnswer.bind(this);
        this.onAnswer = this.onAnswer.bind(this);
        this.onRemoteDescription = this.onRemoteDescription.bind(this);
        this.pullICECandidate = this.pullICECandidate.bind(this);
        this.onLocalICECandidate = this.onLocalICECandidate.bind(this);
        this.onRemoteICECandidate = this.onRemoteICECandidate.bind(this);
        this.onConnectionStateChange = this.onConnectionStateChange.bind(this);

        this.inTrack = null;
        this.outTrack = track;
        this.onTrack = onTrack;
        this.onStatusChange = onStatusChange;
        this.onError = onError;

        console.log("Acquiring ICE server.");

        fetch("/webrtc/ice", {
            method: "POST",
            body: JSON.stringify({})
        })
            .then(res => res.json())
            .then(this.onICE)
            .catch(e => this.onError(e.message));
    }

    connectionStatus(): string
    {
        switch (this.connection.connectionState)
        {
        case "new" :
        case "connecting" :
            return "pending";
        case "connected" :
            return "connected";
        case "failed" :
        case "disconnected" :
        case "closed" :
            return "disconnected";
        }
    }

    private onICE(json: any): void
    {
        if (json.error)
        {
            this.onError(json);
            return;
        }

        var response: WebRTCMessages.ICEResponse = json;

        var config : RTCConfiguration = {
            iceServers: response.servers
        };

        this.connection = new RTCPeerConnection(config);
        this.connection.onicecandidate = this.onLocalICECandidate.bind(this);
        this.connection.ontrack = this.onRemoteTrack.bind(this);
        this.connection.onconnectionstatechange = this.onConnectionStateChange.bind(this);

        console.log("Outgoing audio stream opened.");
        this.connection.addTrack(this.outTrack);

        this.createOffer();
    }

    private createOffer(): void
    {
        console.log("Creating offer.");

        var options: RTCOfferOptions = {
            offerToReceiveAudio: true
        };

        this.connection.createOffer(options)
            .then(this.onOffer)
            .catch(e => this.onError(e.message));
    }

    private onOffer(offer: RTCSessionDescription): void
    {
        console.log("Created offer.");

        this.connection.setLocalDescription(offer)
            .then(this.onLocalDescription)
            .catch(e => this.onError(e.message));
    }

    private onLocalDescription(): void
    {
        console.log("Sending offer.");

        var request: WebRTCMessages.OfferRequest = {
            id: this.id,
            local_description: JSON.stringify(this.connection.localDescription)
        };

        fetch("/webrtc/offer", {
            method: "POST",
            body: JSON.stringify(request)
        });

        this.getAnswer();
    }

    private getAnswer(): void
    {
        console.log("Getting answer.");

        var request: WebRTCMessages.GetAnswerRequest = {
            id: this.id
        }

        fetch("/webrtc/get-answer", {
            method: "POST",
            body: JSON.stringify(request)
        })
            .then(res => res.json())
            .then(this.onAnswer)
            .catch(e => this.onError(e.message));
    }

    private onAnswer(json: any): void
    {
        if (json.error)
        {
            this.onError(json);
            return;
        }

        var response: WebRTCMessages.GetAnswerResponse = json;

        if (response.remote_description)
        {
            console.log("Got answer.");

            this.connection.setRemoteDescription(JSON.parse(response.remote_description))
                .then(this.onRemoteDescription)
                .catch(e => this.onError(e.message));
        }
        else
        {
            setTimeout(this.getAnswer, answerPollInterval);
        }
    }

    private onRemoteDescription(): void
    {
        this.pullICECandidate();
    }

    private pullICECandidate(): void
    {
        var request: WebRTCMessages.PullICECandidateRequest = {
            id: this.id
        };

        fetch("/webrtc/pull-client-ice-candidate", {
            method: "POST",
            body: JSON.stringify(request)
        })
            .then(res => res.json())
            .then(this.onRemoteICECandidate)
            .catch(e => this.onError(e.message));
    }

    private onLocalICECandidate(e: RTCPeerConnectionIceEvent): void
    {
        if (e.candidate)
        {
            console.log("Pushing ICE candidate.");

            var request: WebRTCMessages.PushICECandidateRequest = {
                id: this.id,
                candidate: JSON.stringify(e.candidate)
            };

            fetch("/webrtc/push-server-ice-candidate", {
                method: "POST",
                body: JSON.stringify(request)
            });
        }
    }

    private onRemoteICECandidate(json: any): void
    {
        if (json.error)
        {
            this.onError(json);
            return;
        }

        var response: WebRTCMessages.PullICECandidateResponse = json;

        if (response.candidate)
        {
            console.log("Pulled ICE candidate.");

            this.connection.addIceCandidate(JSON.parse(response.candidate));
        }

        setTimeout(this.pullICECandidate, iceCandidatePollInterval);
    }

    private onRemoteTrack(e: RTCTrackEvent): void
    {
        console.log("Incoming audio stream opened.");

        this.inTrack = e.track;

        this.onTrack(this.inTrack);
    }

    private onConnectionStateChange(e: Event): void
    {
        switch (this.connection.connectionState)
        {
        case "connected" :
            console.log("Connected.");
            break;
        }

        this.onStatusChange();
    }
}
