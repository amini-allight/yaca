/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import Connection from "./connection";

export interface NewRequest
{
    session: string;
}

export interface JoinRequest
{
    session: string;
    username: string;
}

export interface LeaveRequest
{
    session: string;
    username: string;
}

export interface PingRequest
{
    session: string;
    username: string;
}

export interface PingResponse
{
    usernames: Array<string>;
}

export interface ConnectToRequest
{
    session: string;
    src_username: string;
    dst_username: string;
}

export interface ConnectToResponse
{
    connection: Connection;
}
