/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import * as React from "react";
import NewSessionDialog from "./new-session-dialog";
import UsernameDialog from "./username-dialog";
import Session from "./session";
import * as SessionMessages from "./session-messages";

interface VoiceChatProps
{

}

interface VoiceChatState
{
    hash: string;
    username: string;
    errors: Array<string>;
}

export default class VoiceChat extends React.Component<VoiceChatProps, VoiceChatState>
{
    constructor(props)
    {
        super(props);

        this.state = {
            hash: window.location.hash,
            username: "",
            errors: []
        };

        this.onNewSessionRequested = this.onNewSessionRequested.bind(this);
        this.onNew = this.onNew.bind(this);
        this.onUsernameSet = this.onUsernameSet.bind(this);
        this.onJoin = this.onJoin.bind(this);
        this.onBeforeUnload = this.onBeforeUnload.bind(this);
        this.onError = this.onError.bind(this);
    }

    render()
    {
        var session = this.sessionName();
        var username = this.state.username;

        var icon = null;

        var content = null;

        if (session == "")
        {
            icon = (<img className="icon" src="/static/icon.png"/>);
            content = (<NewSessionDialog onDone={this.onNewSessionRequested}/>);
        }
        else
        {
            if (username == "")
            {
                icon = (<img className="icon" src="/static/icon.png"/>);
                content = (<UsernameDialog onDone={this.onUsernameSet}/>);
            }
            else
            {
                icon = null;
                content = (<Session session={session} username={username} onError={this.onError}/>);
            }
        }

        var errorMessages = [];

        for (var i = this.state.errors.length - 1; i >= 0; i--)
        {
            errorMessages.push(<p className="error-message">{this.state.errors[i]}</p>);
        }

        return (
            <div className="voice-chat">
                {icon}
                {content}
                <div className="error-log">
                    {errorMessages}
                </div>
            </div>
        );
    }

    private onNewSessionRequested(name: string): void
    {
        if (name == "")
        {
            return;
        }

        var request : SessionMessages.NewRequest = {
            session: name
        };

        fetch("/session/new", {
            method: "POST",
            body: JSON.stringify(request)
        })
            .then(res => res.json())
            .then((json => this.onNew(json, name)).bind(this))
            .catch(e => this.onError(e.message));
    }

    private onNew(json: any, name: string): void
    {
        if (json.error)
        {
            this.onError(json.error);
            return;
        }

        window.location.hash = "#" + name;
        this.setState({
            hash: window.location.hash
        });
    }

    private onUsernameSet(username: string): void
    {
        if (username == "")
        {
            return;
        }

        var request : SessionMessages.JoinRequest = {
            session: this.sessionName(),
            username: username
        };

        fetch("/session/join", {
            method: "POST",
            body: JSON.stringify(request)
        })
            .then(res => res.json())
            .then((json => this.onJoin(json, username)).bind(this))
            .catch(e => this.onError(e.message));
    }

    private onJoin(json: any, username: string): void
    {
        if (json.error)
        {
            this.onError(json.error);
            return;
        }

        this.setState({
            username: username
        });

        addEventListener("beforeunload", this.onBeforeUnload);
    }

    private onBeforeUnload(e: BeforeUnloadEvent): void
    {
        var request: SessionMessages.LeaveRequest = {
            session: this.sessionName(),
            username: this.state.username
        };

        fetch("/session/leave", {
            method: "POST",
            body: JSON.stringify(request)
        });
    }

    private onError(error: string): void
    {
        this.setState({
            errors: this.state.errors.concat([ error ])
        });
    }

    private sessionName(): string
    {
        if (this.state.hash == "")
        {
            return "";
        }
        else if (this.state.hash == "#")
        {
            return "";
        }
        else
        {
            return this.state.hash.substring(1, this.state.hash.length);
        }
    }
}
