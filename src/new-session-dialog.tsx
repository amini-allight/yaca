/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import * as React from "react";

interface NewSessionDialogProps
{
    onDone: (session: string) => void;
}

interface NewSessionDialogState
{

}

export default class NewSessionDialog extends React.Component<NewSessionDialogProps, NewSessionDialogState>
{
    name: React.RefObject<HTMLInputElement>;

    constructor(props)
    {
        super(props);

        this.name = React.createRef<HTMLInputElement>();

        this.onKeyDown = this.onKeyDown.bind(this);
        this.onDone = this.onDone.bind(this);
    }

    render()
    {
        return (
            <div className="new-session-dialog">
                <span className="dialog-prompt">Create a new session:</span>
                <input autoFocus ref={this.name} className="dialog-line-input" type="text" placeholder="Enter a session name" onKeyDown={this.onKeyDown}/>
                <button className="dialog-button" onClick={this.onDone}>New</button>
            </div>
        );
    }

    private onKeyDown(e: React.KeyboardEvent<HTMLInputElement>): void
    {
        if (e.key != "Enter")
        {
            return;
        }

        this.onDone();
    }

    private onDone(): void
    {
        this.props.onDone(this.name.current.value);
    }
}
