/*
Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.
Copyright (C) 2024 Amini Allight

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
export default class AudioInput
{
    track: MediaStreamTrack;
    onReady: () => void;

    constructor(onReady: () => void, onError: (error: string) => void)
    {
        this.track = null;
        this.onReady = onReady;

        this.onUserMedia = this.onUserMedia.bind(this);

        navigator.mediaDevices.getUserMedia({
            audio: true,
            video: false
        })
            .then(this.onUserMedia)
            .catch(e => onError(e.message));
    }

    muted(): boolean
    {
        return this.track != null && !this.track.enabled;
    }

    mute(): void
    {
        this.track.enabled = false;
    }

    unmute(): void
    {
        this.track.enabled = true;
    }

    private onUserMedia(stream: MediaStream): void
    {
        this.track = stream.getAudioTracks()[0];

        this.onReady();
    }
}
