# Yaca

Yaca (Yet Another Chat Application), a solution for self-hosted WebRTC voice chat.

![](doc/icon.png)

![](doc/mosaic.png)

## Why?

The other ones weren't doing what I wanted, that's really it.

## Security

Yaca does not have any security features in its design. Clients can easily impersonate each other or DDoS the connection establishment mechanism. This is an intentional tradeoff made to keep Yaca simple because it was designed for use by small groups of trusted users. If there is interest in a secured version I may make one at some point but it isn't a priority right now.

## Dependencies

You will need a POSIX-style environment (e.g. Linux, MacOS, FreeBSD or any of the various POSIX environments for Windows) with [Crystal](https://crystal-lang.org/) and [npm](https://www.npmjs.com/) installed.

## Setup

Download this repository then run this to build the web frontend:

```sh
npm install
npm run build
```

And then run this to run the server:

```sh
npm run serve -- 0.0.0.0 3000
```

The application will then be available at `http://127.0.0.1:3000/`.

## HTTPS

You will need to place a web server (e.g. [Apache HTTPD](https://httpd.apache.org/) or [NGINX](https://nginx.org/en/)) in front of Yaca to provide HTTPS if you want Yaca to be usable outside of the localhost loopback. This is required because modern web browsers will not provide microphone access to unencrypted contexts. For Apache HTTPD the configuration looks something like this:

```apache
LoadModule proxy_module modules/mod_proxy.so
LoadModule ssl_module modules/mod_ssl.do

<VirtualHost *:443>
    ServerName yaca.example.com
    ProxyPreserveHost on
    ProxyPass / http://127.0.0.1:3000/
    ProxyPassReverse / http://127.0.0.1:3000/
    SSLEngine on
    SSLCertificateFile "/etc/letsencrypt/live/yaca.example.com/fullchain.pem"
    SSLCertificateKeyFile "/etc/letsencrypt/live/yaca.example.com/privkey.pem"
</VirtualHost>

<VirtualHost *:80>
    ServerName yaca.example.com
    Redirect / https://yaca.example.com/
</VirtualHost>
```

And for NGINX:

```nginx
server {
    server_name yaca.example.com;
    listen 443 ssl;
    listen [::]:443 ssl;
    proxy_pass http://127.0.0.1:3000;
    ssl on;
    ssl_certificate "/etc/letsencrypt/live/yaca.example.com/fullchain.pem";
    ssl_certificate_key "/etc/letsencrypt/live/yaca.example.com/privkey.pem";
}

server {
    server_name yaca.example.com;
    listen 80;
    listen [::]:80;
    rewrite ^ https://$server_name$request_uri? permanent;
}
```

The above examples assume you have a TLS certificate provided by [Let's Encrypt](https://letsencrypt.org/), you can learn more about how to set that up [here](https://certbot.eff.org/).

## NAT Traversal

Yaca uses WebRTC to establish connections between peers and WebRTC in turn uses either STUN or TURN servers to bypass NAT firewalls (e.g. the firewall of your home router) that would otherwise prevent peers from connecting to each other. STUN servers only orchestrate hole-punching between clients and so have a trivial bandwidth cost, meaning public ones are available, but they also have a chance of failure with particularly restrictive NATs. TURN servers on the other hand relay traffic between peers and so are guaranteed to work in effectively all situations, but have a non-trivial bandwidth cost which prevents free ones from existing.

By default Yaca will use Google's free STUN server at `stun:stun.l.google.com:19302`. If you want to use a different STUN or TURN server, or multiple of them, you can place a configuration file in the following location:

 Operating System | Location
------------------|----------
 MacOS            | `$HOME/Library/Application Support/Yaca/config.json`
 Linux & Others   | `$HOME/.config/yaca/config.json`
 Windows          | `%APPDATA%/Yaca/config.json`

The configuration file has contents in the following format:

```json
[
    {
        "urls": [ "stun:stun.l.google.com:19302" ],
        "username": "",
        "credential": ""
    },
    {
        "urls": [ "turn:example.com:3478" ],
        "username": "myusername",
        "credential": "mypassword"
    }
]
```

The `username` and `credential` fields are only meaningful for TURN servers.

To set up a TURN server install [coturn](https://github.com/coturn/coturn) and write a configuration file called `turnserver.conf` with the following contents:

```
lt-cred-mech
user=myusername:mypassword
```

Note that the TURN username and password are shared between all clients using your Yaca instance and as such you only need one set of credentials. Also note that these credentials are sent to the clients without any restrictions on their use: nefarious clients may choose to save them and use them to later route arbitrary traffic via your TURN server.

Once you've written the configuration file run the TURN server with:

```sh
turnserver -c turnserver.conf --log-file stdout
```

Yaca should now be configured to support routing for effectively all clients (the only exception I am aware of being situations where one or more clients cannot see the TURN server which is almost always the result of misconfiguration e.g. the TURN server being only accessible within a local network).

## Usage

To use Yaca navigate to `https://yaca.example.com/` and enter a name for your new room. A server can support any number of rooms and rooms are automatically deleted after a short period of inactivity. Once you create a room your URL will change to `https://yaca.example.com/#myroom`. Enter a username for yourself and then send this URL to the other participants to invite them to join the room.

## Credit & License

Developed by Amini Allight. Licensed under the AGPL 3.0.

This project contains files from the Open Sans font under their license.
